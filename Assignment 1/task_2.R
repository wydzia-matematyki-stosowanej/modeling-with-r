# a
k <- 1:100
a <- sum((3 * k ^ 3) / (2 * k ^ 2 + 1))
print(a)

# b
k <- 1:30
b <- (1 / pi ^ (1 / 2)) * sum(((k ^ 2 - 1) ^ 2) / exp(k ^ 2))
print(b)